<?php
/**
 * Created by PhpStorm.
 * User: dam1r89
 * Date: 7/2/16
 * Time: 12:33 PM
 */

namespace App;


use GuzzleHttp\Client;

class Bitbucket
{

    private $client;

    private $username;
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;

        $this->client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/',
            'auth' => [$this->username, $this->password]
        ]);
    }

    public function repositories($role = Role::OWNER)
    {
        $out = [];
        foreach (new PaginatedResource($this->client, 'repositories', [
            'role' => $role
        ]) as $repo) {
            $out[] = "$repo->full_name ($repo->name)";
        }
        return $out;
    }

    public function addWebhook($repo, $name, $description, $url, $events = ['repo:push'], $active = true)
    {
        $this->client->post("repositories/$repo/hooks", [
            'json' => compact('name', 'description', 'url', 'events', 'active')
        ]);
    }

    public function listWebhooks($repo)
    {

        $out = [];
        foreach (new PaginatedResource($this->client, "repositories/$repo/hooks") as $webhook) {
            $out[] = "{$webhook->subject->name} ($webhook->url)";
        }
        return $out;
    }

    public function addDeploymentKey($repo, $key, $description)
    {
        $this->client->post("https://api.bitbucket.org/1.0/repositories/$repo/deploy-keys", [
            'json' => compact('key', 'description')
        ]);
    }

    public function info($repo)
    {
        return json_decode($this->client->get('repositories/' . $repo)->getBody());
    }

    /**
     * Create or update repository
     */
    public function create($repo, $description = null, $type = 'git', $is_private = true)
    {
        $response = $this->client->put('repositories/' . $this->username . '/' . $repo, [
            'json' => compact('description', 'type', 'is_private')
        ]);

        return json_decode($response->getBody());
    }

    public function putFile($repo, $file, $name = null)
    {
//        $name = $name ?: pathinfo($file, PATHINFO_FILENAME);
        $res = $this->client->post('repositories/' . $this->username . '/' . $repo . '/downloads', [
            'body' => fopen($file, 'r'),
            'headers' => [
                'Content-Type' => 'multipart/form-data'
            ]
        ]);
        return json_decode($res->getBody());
    }

}
