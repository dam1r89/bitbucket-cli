<?php
/**
 * Created by PhpStorm.
 * User: dam1r89
 * Date: 7/2/16
 * Time: 12:38 PM
 */

namespace App;


use GuzzleHttp\Client;

class PaginatedResource implements \Iterator
{

    private $array = [];
    private $position = 0;
    private $nextUrl = [];
    private $options;
    private $resource;
    /**
     * @var Client
     */
    private $client;

    public function __construct($client, $resource, $options = [])
    {
        $this->client = $client;
        $this->options = $options;
        $this->resource = $resource;
        $this->position = 0;
        // Call first time to get initial results
        $this->getNext();
    }


    public function current()
    {
        if (!isset($this->array[$this->position])) {
            $this->getNext();
        }
        return $this->array[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;

    }

    public function valid()
    {
        return isset($this->array[$this->position]) || $this->nextUrl;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @param $resource
     * @return array
     */
    private function getNext()
    {
        $results = [];

//        var_dump(array_merge($this->options, $this->nextUrl));
        $repos = json_decode($this->client->get($this->resource, [
            'query' => array_merge($this->options, $this->nextUrl)
        ])->getBody());

        foreach ($repos->values as $item) {
            $results[] = $item;
        }

        if (isset($repos->next)) {

            parse_str(parse_url($repos->next, PHP_URL_QUERY), $parts);

            $this->nextUrl = $parts;

        } else {
            $this->nextUrl = null;
        }


        $this->array = array_merge($this->array, $results);
    }

}
