<?php

namespace App\Console\Commands;

use App\Bitbucket;
use App\Role;
use Illuminate\Console\Command;

class BitbucketCommand extends Command
{
    protected $signature = 'console {--username=} {--password=} {--repo=}';

    protected $description = 'Interactive console for controlling bitbucket repositories.';

    protected $client;
    protected $currentRepo;
    private $conf;

    public function handle()
    {

        $this->loadConfig();

        $username = $this->get('username');

        $this->currentRepo = $this->option('repo');

        $bb = new Bitbucket($username, $this->get('password'));


        while (true) {

            $command = $this->ask('[' . ($this->currentRepo ?: 'no repo selected') . ']', ' ');
            try {

                switch ($command) {
                    case 'help':
                        $this->info('Commands: help, list, create, use, add webhook, list webhooks, add key, find, info');
                        break;

                    case 'create':

                        $response = $bb->create($this->ask('Repository name'));
                        foreach ($response->links->clone as $clone) {
                            if ($clone->name === 'ssh') {
                                $link = substr($clone->href, 3);
                                $this->info("New Project:\n\ngit init\ngit remote add origin git@$link\n\nExisting project:\n\ngit remote add origin git@$link\ngit push -u origin --all\ngit push origin --tags");
                            }
                        }
                        $this->currentRepo = $response->name;
                        break;
                    case 'info':
                        $this->currentRepo = $this->currentRepo ?: $this->ask('Select repo');
                        $response = $bb->info($this->currentRepo);
                        $this->info('Description' . $response->description);
                        foreach ($response->links->clone as $clone) {
                            $this->info('clone: ' . $clone->name . ' - ' . $clone->href);
                        }
                        break;
                    case 'find':
                        $kw = $this->ask('Keyword');
                        foreach ($list = $bb->repositories($this->ask('Which role (owner|admin|contributor|member)', Role::MEMBER)) as $item) {
                            if (strpos($item, $kw) !== false) {
                                $ok = $this->confirm('Ok ' . $item, false);
                                if ($ok) {
                                    $this->currentRepo = explode(' ', $item)[0];
                                    break;
                                }
                            }
                        }
                        break;
                    case 'list':
                        foreach ($list = $bb->repositories($this->ask('Which role (owner|admin|contributor|member)', Role::OWNER)) as $item) {
                            $this->info($item);
                        }
                        break;
                    case 'use':
                        $this->currentRepo = $this->ask('Select current repo', '');
                        break;
                    case 'add webhook':
                        $bb->addWebhook($this->currentRepo, $this->ask('Name'), $this->ask('Description'), $this->ask('Url'));
                        break;
                    case 'list webhooks':
                        $wh = $bb->listWebhooks($this->currentRepo);
                        if (count($wh) == 0) {
                            $this->info('No webhooks');
                        }
                        foreach ($wh as $hook) {
                            $this->info($hook);
                        }
                        break;
                    case 'add key':
                        $keyPath = $this->ask('Enter key path', getenv('HOME') . '/.ssh/id_rsa.pub');
                        if (!is_file($keyPath)) {
                            $this->info("Path '$keyPath' doesn't exist. Try generating key pair with `ssh-keygen -t rsa`");
                            break;
                        }
                        $key = file_get_contents($keyPath);
                        $bb->addDeploymentKey($this->currentRepo, $key, $this->ask('Description'));
                        break;
                    case 'upload':
                        $this->error('Not Implemented');
                        break;
                        $bb->putFile($this->currentRepo, $this->ask('Enter path name.'));
                        break;
                    case 'exit':
                    case 'quit':
                        return;
                    default:
                        $this->warn('Enter valid command or type "help"');
                }
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }

        }

    }

    private function get($parameter, $secret = false)
    {
        if ($this->option($parameter)) {
            return $this->option($parameter);
        }
        if (isset($this->conf[$parameter])) {
            return $this->conf[$parameter];
        }
        if ($secret) {
            return $this->secret("Enter $parameter");
        }
        return $this->ask("Enter $parameter");
    }

    private function loadConfig()
    {
        $configPath = getenv('HOME') . '/.bitbucket';
        if (file_exists($configPath)) {
            if (fileperms($configPath) & 0044) {
                $this->warn("Config file is readable by other users\nChange permissions");
            }
            $this->conf = parse_ini_file($configPath);
        }
    }

}
