<?php

namespace App\Console\Commands;

use App\Bitbucket;
use App\Role;
use Illuminate\Console\Command;

class ListCommand extends Command
{
    protected $signature = 'repos {--username=} {--repo=}';

    protected $description = 'Get the list of bitbucket repositories.';
    protected $client;
    protected $currentRepo;

    public function handle()
    {

        $username = $this->option('username') ?: $this->ask('Username');

        $this->currentRepo = $this->option('repo');

        $bb = new Bitbucket($username, getenv('BB_PASSWORD') ?: $this->secret('password'));
        $list = $bb->repositories($this->ask('Which role (owner|admin|contributor|member)', Role::OWNER));
        $this->askWithCompletion('Choose', $list);


    }

}
