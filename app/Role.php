<?php
/**
 * Created by PhpStorm.
 * User: dam1r89
 * Date: 7/4/16
 * Time: 10:38 AM
 */

namespace App;


class Role
{
    const OWNER = 'owner';
    const ADMIN = 'admin';
    const CONTRIBUTOR = 'contributor';
    const MEMBER = 'member';

}
